#!/bin/sh -e

path=$(pwd)

if [ ! -d /mnt/upgrade ];
then
	mkdir /mnt/upgrade
fi

mount -t jffs2 /dev/mtdblock4 /mnt/upgrade/
cd /mnt/upgrade/upgrade
rm -rf ./*
cd $path

cp -rf ./* /mnt/upgrade/upgrade

flash_erase /dev/mtd2 0x0 0x1 >/dev/null 2>&1
nandwrite -p -s 0x0 /dev/mtd2 /mnt/upgrade/upgrade/upgrade-marker.bin >/dev/null 2>&1

sync

umount /dev/mtdblock4

#/sbin/reboot -f &

